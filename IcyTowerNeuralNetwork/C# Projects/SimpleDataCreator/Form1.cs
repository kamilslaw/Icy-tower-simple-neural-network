﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleDataCreator
{
    public partial class Form1 : Form
    {
        public static List<FloorFrame> FloorFrames { get; private set; } = new List<FloorFrame>();
        public static List<JumpFrame> JumpFrames { get; private set; } = new List<JumpFrame>();

        private List<string> AlreadyCheckedDirectories = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSelectData_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog() { ShowNewFolderButton = false, SelectedPath = @"D:\Simple Data", RootFolder = Environment.SpecialFolder.MyComputer })
            {
                if (fbd.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    var directories = Directory.GetDirectories(fbd.SelectedPath);
                    progressBar1.Maximum = directories.Length;
                    progressBar1.Value = 0;
                    foreach (var directory in directories)
                    {
                        if (!AlreadyCheckedDirectories.Contains(directory))
                        {
                            AlreadyCheckedDirectories.Add(directory);
                            string[] imageFiles = Directory.GetFiles(directory)
                                                           .Where(f => f.ToLower().EndsWith(".png"))
                                                           .OrderBy(f => int.Parse(Path.GetFileNameWithoutExtension(f)))
                                                           .ToArray();
                            progressBar2.Value = 0;
                            progressBar2.Maximum = imageFiles.Length;

                            Frame prevFrame = null;
                            for (int i = 0; i < imageFiles.Length; i++)
                            {
                                Frame resultFrame = Analyzer.AnalyzeImage(imageFiles[i], prevFrame);
                                if (resultFrame != null)
                                {
                                    prevFrame = resultFrame;

                                    if (resultFrame is JumpFrame jf) JumpFrames.Add(jf);
                                    else if (resultFrame is FloorFrame ff) FloorFrames.Add(ff);
                                }

                                progressBar2.PerformStep();
                            }
                        }
                        progressBar1.PerformStep();
                    }

                    FloorFrames = FloorFrames.Where(Analyzer.ValidFloorFramePredicate).ToList(); // !!!

                    buttonSeeResults.Text = $"See Results{Environment.NewLine}{FloorFrames.Count + JumpFrames.Count} frames analized{Environment.NewLine}({FloorFrames.Count} on floor)";
                }
            }
        }

        private void buttonSeeResults_Click(object sender, EventArgs e) => new ResultForm().ShowDialog();

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Floor frames data|*.csv";
            saveFileDialog.Title = "Save floor frames";           
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var sb = new StringBuilder();
                //sb.AppendLine();
                FloorFrames.ForEach(f => sb.AppendLine(string.Join(",", f.ToCsvRow())));
                File.WriteAllText(saveFileDialog.FileName, sb.ToString());
            }

            saveFileDialog.Filter = "Jumping frames data|*.csv";
            saveFileDialog.Title = "Save jumping frames";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var sb = new StringBuilder();
                //sb.AppendLine();
                JumpFrames.ForEach(f => sb.AppendLine(string.Join(",", f.ToCsvRow())));
                File.WriteAllText(saveFileDialog.FileName, sb.ToString());
            }
        }
    }
}
