﻿using System.Globalization;

namespace SimpleDataCreator
{
    public enum OutputKey
    {
        None = 0,
        Left = 1,
        Right = 2,
        Space = 3,
        SpaceLeft = 4,
        SpaceRight = 5
    }

    public abstract class Frame
    {
        private static int FramesCounter;

        // Extra data
        public int Id { get; } = ++FramesCounter;
        public int TargetFloorX { get; set; }
        public string FilePath { get; set; }
        public int GuyX { get; set; }
        public int GuyY { get; set; }
        // Input data
        public int TargetFloorLength { get; set; }
        public int DistanceX { get; set; }
        public int DX { get; set; }
        public int DY { get; set; }
        public int DX2 { get; set; }
        public int DY2 { get; set; }
        public int DX3 { get; set; }
        public int DY3 { get; set; }
        public int DX4 { get; set; }
        public int DY4 { get; set; }
        public int DX5 { get; set; }
        public int DY5 { get; set; }
        public int DX6 { get; set; }
        public int DY6 { get; set; }
        public int DX7 { get; set; }
        public int DY7 { get; set; }
        public int DX8 { get; set; }
        public int DY8 { get; set; }
        public OutputKey LastKey { get; set; }
        public OutputKey LastKey2 { get; set; }
        public OutputKey LastKey3 { get; set; }
        public OutputKey LastKey4 { get; set; }
        public OutputKey LastKey5 { get; set; }
        public OutputKey LastKey6 { get; set; }
        public OutputKey LastKey7 { get; set; }
        public OutputKey LastKey8 { get; set; }
        public int GuyDistanceToStartX { get => GuyX - TargetFloorX; }
        public int GuyDistanceToEndX { get => GuyX - (TargetFloorX + TargetFloorLength); }
        public int LastGuyDistanceToStartX { get; set; }
        public int LastGuyDistanceToStartX2 { get; set; }
        public int LastGuyDistanceToStartX3 { get; set; }
        public int LastGuyDistanceToStartX4 { get; set; }
        public int LastGuyDistanceToEndX { get; set; }
        public int LastGuyDistanceToEndX2 { get; set; }
        public int LastGuyDistanceToEndX3 { get; set; }
        public int LastGuyDistanceToEndX4 { get; set; }
        // Output data
        public OutputKey Key { get; set; }

        // Input data, but for floor frame!!!!11!1
        public int PositionOnFloor { get; set; }
        public double LastPositionOnFloor { get; set; }
        public double LastPositionOnFloor2 { get; set; }
        public double LastPositionOnFloor3 { get; set; }
        public double LastPositionOnFloor4 { get; set; }

        public abstract object[] ToDataRow();
        public abstract object[] ToCsvRow();
        public abstract string ToNeuralNetworkRow();
    }

    public class FloorFrame : Frame
    {
        // Extra data
        public int FloorX { get; set; }
        // Input data
        public int FloorLength { get; set; }
        public int PositionOnFloorPercent { get => FloorLength == 0 ? 0 : PositionOnFloor * 100 / FloorLength; }

        public override object[] ToDataRow()
        {
            // return new object[] { Id, DX, DY, FloorLength, PositionOnFloor, TargetFloorLength, DistanceX, "", Key };
            return new object[] { Id, DX, DY, FloorLength, PositionOnFloorPercent, GuyDistanceToStartX, GuyDistanceToEndX, "", Key };
        }
        public override object[] ToCsvRow()
        {
            //var obj = new object[] { (int)Key, DX, FloorLength, PositionOnFloor, TargetFloorLength, DistanceX};
            //var obj = new object[] { (int)Key - 1, DX, FloorLength, PositionOnFloorPercent, GuyDistanceToStartX, GuyDistanceToEndX };
            //var obj = new object[] { (int)Key, DX, DX2, DX3, DX4, FloorLength, PositionOnFloorPercent, GuyDistanceToStartX, GuyDistanceToEndX,
            //                         (int)LastKey, (int)LastKey2, (int)LastKey3, (int)LastKey4 };
            var obj = new object[] { (int)Key, DX, DX2, DX3, DX4, DX5, DX6, DX7, DX8, FloorLength, PositionOnFloorPercent, GuyX, TargetFloorX, TargetFloorLength,
                                    GuyDistanceToStartX, LastGuyDistanceToStartX, LastGuyDistanceToStartX2, LastGuyDistanceToStartX3, LastGuyDistanceToStartX4,
                                    GuyDistanceToEndX, LastGuyDistanceToEndX, LastGuyDistanceToEndX2, LastGuyDistanceToEndX3, LastGuyDistanceToEndX4,
                                    LastPositionOnFloor, LastPositionOnFloor2, LastPositionOnFloor3,  LastPositionOnFloor4,
                                    (int)LastKey, (int)LastKey2, (int)LastKey3, (int)LastKey4, /*(int)LastKey5, (int)LastKey6, (int)LastKey7, (int)LastKey8*/ };
            return obj;
        }
        public override string ToNeuralNetworkRow()
        {
            //var obj = new object[] { 1, DX, FloorLength, PositionOnFloor, TargetFloorLength, DistanceX };
            //var obj = new object[] { (int)Key - 1, DX, FloorLength, PositionOnFloorPercent, GuyDistanceToStartX, GuyDistanceToEndX };
            //var obj = new object[] { 1, DX, DX2, DX3, DX4, FloorLength, PositionOnFloorPercent, GuyDistanceToStartX, GuyDistanceToEndX, (int)LastKey, (int)LastKey2, (int)LastKey3, (int)LastKey4 };
            var obj = new object[] { 1,  DX, DX2, DX3, DX4, /*DX5, DX6, DX7, DX8,*/ FloorLength, PositionOnFloorPercent, GuyX, TargetFloorX, TargetFloorLength,
                                    GuyDistanceToStartX, /*LastGuyDistanceToStartX, LastGuyDistanceToStartX2, LastGuyDistanceToStartX3, LastGuyDistanceToStartX4,*/
                                    GuyDistanceToEndX, /*LastGuyDistanceToEndX, LastGuyDistanceToEndX2, LastGuyDistanceToEndX3, LastGuyDistanceToEndX4,*/
                                    //LastPositionOnFloor, LastPositionOnFloor2, LastPositionOnFloor3,  LastPositionOnFloor4,
                                    (int)LastKey, (int)LastKey2, (int)LastKey3, (int)LastKey4 };
            return string.Join(",", obj);
        }
    }

    public class JumpFrame : Frame
    {
        // Input data
        public int DistanceY { get; set; }

        public override object[] ToDataRow()
        {
            return new object[] { Id, DX, DY, "", "", TargetFloorLength, DistanceX, DistanceY, Key };
        }
        public override object[] ToCsvRow()
        {
            return new object[] { (int)Key, DX, DX2, DX3, DX4, DY, DY2, DY3, DY4, TargetFloorLength, GuyDistanceToStartX, DistanceY,
                LastGuyDistanceToStartX, LastGuyDistanceToStartX2, LastGuyDistanceToStartX3, LastGuyDistanceToStartX4,
                (int)LastKey, (int)LastKey2, (int)LastKey3, (int)LastKey4 };
        }
        public override string ToNeuralNetworkRow()
        {
            var obj = new object[] { 0, DX, DX2, DX3, DX4, DY, DY2, DY3, DY4, TargetFloorLength, GuyDistanceToStartX, DistanceY,
                LastGuyDistanceToStartX, LastGuyDistanceToStartX2, LastGuyDistanceToStartX3, LastGuyDistanceToStartX4,
                (int)LastKey, (int)LastKey2, (int)LastKey3, (int)LastKey4 };
            return string.Join(",", obj);
        }
    }
}
