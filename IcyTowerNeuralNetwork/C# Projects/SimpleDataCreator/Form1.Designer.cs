﻿namespace SimpleDataCreator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSelectData = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonSeeResults = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // buttonSelectData
            // 
            this.buttonSelectData.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectData.Location = new System.Drawing.Point(12, 12);
            this.buttonSelectData.Name = "buttonSelectData";
            this.buttonSelectData.Size = new System.Drawing.Size(91, 66);
            this.buttonSelectData.TabIndex = 0;
            this.buttonSelectData.Text = "Select Data";
            this.buttonSelectData.UseVisualStyleBackColor = true;
            this.buttonSelectData.Click += new System.EventHandler(this.buttonSelectData_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 84);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(351, 30);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 1;
            // 
            // buttonSeeResults
            // 
            this.buttonSeeResults.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSeeResults.Location = new System.Drawing.Point(109, 12);
            this.buttonSeeResults.Name = "buttonSeeResults";
            this.buttonSeeResults.Size = new System.Drawing.Size(157, 66);
            this.buttonSeeResults.TabIndex = 2;
            this.buttonSeeResults.Text = "See Results\r\n0 frames analized\r\n(0 on floor)";
            this.buttonSeeResults.UseVisualStyleBackColor = true;
            this.buttonSeeResults.Click += new System.EventHandler(this.buttonSeeResults_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(272, 12);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(91, 66);
            this.buttonSave.TabIndex = 3;
            this.buttonSave.Text = "Save Results";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(12, 120);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(351, 30);
            this.progressBar2.Step = 1;
            this.progressBar2.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar2.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(375, 162);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonSeeResults);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonSelectData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Icy Tower Simple Data Creator";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonSelectData;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button buttonSeeResults;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.ProgressBar progressBar2;
    }
}

