﻿using System;
using System.Drawing;

namespace SimpleDataCreator
{
    public class Analyzer
    {
        private const int X_RIGHT_BORDER = 565;
        private const int X_LEFT_BORDER = 76;
        private const int Y_LEFT_BORDER = 27;
        private const int Y_RIGHT_BORDER = 505;
        private const int DISTANCE_TO_NEXT_FLOOR = 80;
        private const int SERCHING_FLOOR_HEIGHT = 30;
        private readonly static Color STANDING_GUY_COLOR = Color.FromArgb(109, 81, 85);
        private readonly static Color STANDING_GUY_COLOR_HEAD = Color.FromArgb(121, 101, 105);
        private readonly static Color FLYING_GUY_COLOR = Color.FromArgb(0, 255, 255);
        private readonly static Color BASIC_FLOOR_BORDER_COLOR = Color.FromArgb(16, 16, 16);

        public static Frame AnalyzeImage(string filePath, Frame prevFrame)
        {
            using (var image = new Bitmap(filePath))
            {
                return AnalyzeImage(image, prevFrame, filePath: filePath);
            }
        }

        public static Frame AnalyzeImage(Bitmap image, Frame prevFrame, bool isReplay = true, string filePath = null)
        {
            (int guyX, int guyY, bool isOnFloor) = GetGuyPosition(image, prevFrame, isReplay);
            Frame frame = isOnFloor ? (Frame)AnalyzeFloorFrame(image, guyX, guyY, prevFrame, isReplay) : AnalyzeJumpFrame(image, guyX, guyY, prevFrame, isReplay);
            if (frame != null)
            {
                frame.FilePath = filePath;
                frame.GuyX = guyX;
                frame.GuyY = guyY;
                
                if (prevFrame != null)
                {
                    frame.DX2 = prevFrame.DX;
                    frame.DY2 = prevFrame.DY;
                    frame.DX3 = prevFrame.DX2;
                    frame.DY3 = prevFrame.DY2;
                    frame.DX4 = prevFrame.DX3;
                    frame.DY4 = prevFrame.DY3;
                    frame.DX5 = prevFrame.DX4;
                    frame.DY5 = prevFrame.DY4;
                    frame.DX6 = prevFrame.DX5;
                    frame.DY6 = prevFrame.DY5;
                    frame.DX7 = prevFrame.DX6;
                    frame.DY7 = prevFrame.DY6;
                    frame.DX8 = prevFrame.DX7;
                    frame.DY8 = prevFrame.DY7;
                    frame.LastKey = prevFrame.Key;
                    frame.LastKey2 = prevFrame.LastKey;
                    frame.LastKey3 = prevFrame.LastKey2;
                    frame.LastKey4 = prevFrame.LastKey3;
                    frame.LastKey5 = prevFrame.LastKey4;
                    frame.LastKey6 = prevFrame.LastKey5;
                    frame.LastKey7 = prevFrame.LastKey6;
                    frame.LastKey8 = prevFrame.LastKey7;
                    frame.LastGuyDistanceToStartX = prevFrame.GuyDistanceToStartX;
                    frame.LastGuyDistanceToStartX2 = prevFrame.LastGuyDistanceToStartX;
                    frame.LastGuyDistanceToStartX3 = prevFrame.LastGuyDistanceToStartX2;
                    frame.LastGuyDistanceToStartX4 = prevFrame.LastGuyDistanceToStartX3;
                    frame.LastGuyDistanceToEndX = prevFrame.GuyDistanceToEndX;
                    frame.LastGuyDistanceToEndX2 = prevFrame.LastGuyDistanceToEndX;
                    frame.LastGuyDistanceToEndX3 = prevFrame.LastGuyDistanceToEndX2;
                    frame.LastGuyDistanceToEndX4 = prevFrame.LastGuyDistanceToEndX3;
                    frame.LastPositionOnFloor = prevFrame.PositionOnFloor;
                    frame.LastPositionOnFloor2 = prevFrame.LastPositionOnFloor;
                    frame.LastPositionOnFloor3 = prevFrame.LastPositionOnFloor2;
                    frame.LastPositionOnFloor4 = prevFrame.LastPositionOnFloor3;
                }
            }
            return frame;
        }

        public static bool ValidFloorFramePredicate(FloorFrame frame) => frame.Key != OutputKey.None || frame.DX != 0;

        private static FloorFrame AnalyzeFloorFrame(Bitmap image, int guyX, int guyY, Frame prevFrame, bool isReplay)
        {
            FloorFrame result = new FloorFrame();
            // Key
            if (isReplay) result.Key = GetKey(image, true);
            // DX && DY
            if (prevFrame != null) // && !(prevFrame is JumpFrame))
            {
                result.DX = guyX - prevFrame.GuyX;
                result.DY = prevFrame.GuyY - guyY;
                if (isReplay && result.DX == 0 && result.DX2 == 0 && result.DX3 == 0 && result.DX4 == 0 && result.Key == OutputKey.None) result.Key = OutputKey.Space; // !!!!!  TESTOWE
            }
            // Position on the floor & floor size
            if (prevFrame == null)
            {
                result.FloorX = X_LEFT_BORDER;
                result.FloorLength = X_RIGHT_BORDER - X_LEFT_BORDER;
                result.PositionOnFloor = guyX - X_LEFT_BORDER;
            }
            else if (prevFrame is FloorFrame ff)
            {
                result.FloorX = ff.FloorX;
                result.FloorLength = ff.FloorLength;
                result.PositionOnFloor = guyX - result.FloorX;
            }
            else if (prevFrame is JumpFrame jf)
            {
                (int start, int end) = GetFloorInfo(image, guyY + 3);
                result.FloorX = start;
                result.FloorLength = end - start;
                result.PositionOnFloor = guyX - result.FloorX;
            }
            // Size of the next floor & position x of the next floor
            if (prevFrame is FloorFrame)
            {
                result.TargetFloorLength = prevFrame.TargetFloorLength;
                result.TargetFloorX = prevFrame.TargetFloorX;
                result.DistanceX = prevFrame.DistanceX;
            }
            else
            {
                (int start, int end) = GetFloorInfo(image, guyY + 3 - DISTANCE_TO_NEXT_FLOOR);
                result.TargetFloorLength = end - start;
                result.TargetFloorX = start;
                result.DistanceX = result.FloorX - start;
            }
            // !! Update the prevoius frame !!
            //if (isReplay && prevFrame is FloorFrame && prevFrame.Key == OutputKey.None && result.Key != OutputKey.None) prevFrame.Key = result.Key;
            // Check if duplicated
            if (isReplay && prevFrame != null && prevFrame is FloorFrame floorFrame && prevFrame.Key == result.Key && floorFrame.PositionOnFloor == result.PositionOnFloor)
                return null;
            //
            return result;
        }

        private static JumpFrame AnalyzeJumpFrame(Bitmap image, int guyX, int guyY, Frame prevFrame, bool isReplay)
        {            
            JumpFrame result = new JumpFrame();
            // Key
            if (isReplay) result.Key = GetKey(image, false);
            // DX && DY
            if (prevFrame != null)
            {
                result.DX = guyX - prevFrame.GuyX;
                result.DY = prevFrame.GuyY - guyY;
            }
            // Size of the next floor 
            result.TargetFloorLength = prevFrame.TargetFloorLength;
            // Distance x to the next floor
            result.TargetFloorX = prevFrame.TargetFloorX;
            result.DistanceX = guyX - result.TargetFloorX;
            // Distance y to the next floor
            int y = 0;
            while (Math.Abs(y) < SERCHING_FLOOR_HEIGHT)
            {
                if (guyY + y >= Y_LEFT_BORDER && guyY + y <= Y_RIGHT_BORDER && image.GetPixel(result.TargetFloorX, guyY + y) == BASIC_FLOOR_BORDER_COLOR)
                {
                    result.DistanceY = y;
                    break;
                }
                y += result.DY >= 0 ? -1 : 1;
            }
            // !! Update the prevoius frame !!
            if (isReplay && prevFrame is FloorFrame && prevFrame.Key < OutputKey.Space) prevFrame.Key = prevFrame.Key == OutputKey.Left ? OutputKey.SpaceLeft : 
                                                                                        prevFrame.Key == OutputKey.Right ? OutputKey.SpaceRight : 
                                                                                        OutputKey.Space;
            // Check if duplicated
            if (prevFrame is JumpFrame jf && prevFrame.Key == result.Key && jf.DistanceX == result.DistanceX && jf.DistanceY == result.DistanceY)
                return null;
            //
            return result;
        }

        private static (int, int, bool) GetGuyPosition(Bitmap image, Frame prevFrame, bool isReplay)
        {
            if (prevFrame == null && !isReplay) return (187, 457, true); // First frame

            int guyX = 0, guyY = 0;
            bool isOnFloor = false;

            // Checking order (1 is previous position)
            //
            //  X 4 4 4 4 4 X
            //  4 X 3 3 3 X 4
            //  4 3 X 2 X 3 4
            //  4 3 2 1 2 3 4
            //  4 3 X 2 X 3 4
            //  4 X 3 3 3 X 4
            //  X 4 4 4 4 4 X
            //
            int px = prevFrame?.GuyX ?? 187;
            int py = prevFrame?.GuyY ?? 457;
            if (!checkPixel(px, py))
            {
                int d = 1;
                for (int i = 1; i < 500; i += 2)
                {
                    for (int x = px - d + 1; x < px + d; x++) if (checkPixel(x, py + d)) goto End;
                    for (int x = px - d + 1; x < px + d; x++) if (checkPixel(x, py - d)) goto End;
                    for (int y = py - d + 1; y < py + d; y++) if (checkPixel(px + d, y)) goto End;
                    for (int y = py - d + 1; y < py + d; y++) if (checkPixel(px - d, y)) goto End;
                    d++;
                }
            }

            End:
            return (guyX, guyY, isOnFloor);

            bool checkPixel(int x, int y)
            {
                if (y < Y_LEFT_BORDER || y > Y_RIGHT_BORDER || x < X_LEFT_BORDER || x > X_RIGHT_BORDER) return false;
                var pixel = image.GetPixel(x, y);
                if (pixel == STANDING_GUY_COLOR || pixel == FLYING_GUY_COLOR)
                {
                    if (pixel == STANDING_GUY_COLOR)
                    {
                        isOnFloor = true;
                        while (image.GetPixel(--x, y) == STANDING_GUY_COLOR) ;
                        x++;
                    }
                    guyX = x;
                    guyY = y;
                    return true;
                }
                return false;
            }
        }

        private static OutputKey GetKey(Bitmap image, bool isOnFloor)
        {
            if (isOnFloor)
            {
                if (image.GetPixel(610, 448).R == 134 && image.GetPixel(600, 448).R == 134) return OutputKey.SpaceLeft;
                if (image.GetPixel(610, 448).R == 134 && image.GetPixel(620, 448).R == 134) return OutputKey.SpaceRight;
                if (image.GetPixel(610, 448).R == 134) return OutputKey.Space;
                else if (image.GetPixel(600, 448).R == 134) return OutputKey.Left;
                else if (image.GetPixel(620, 448).R == 134) return OutputKey.Right;
            }
            else
            {
                if (image.GetPixel(600, 448).R == 134) return OutputKey.Left;
                else if (image.GetPixel(620, 448).R == 134) return OutputKey.Right;
            }
            return OutputKey.None;
        }

        private static (int, int) GetFloorInfo(Bitmap image, int y)
        {
            bool isStartSet = false;
            int start = X_LEFT_BORDER;
            int end = X_RIGHT_BORDER; // By default we use the longest floor
            for (int x = X_LEFT_BORDER; x <= X_RIGHT_BORDER; x++)
            {
                if (image.GetPixel(x, y) == BASIC_FLOOR_BORDER_COLOR)
                {
                    if (!isStartSet)
                    {
                        isStartSet = true;
                        start = x;
                        x += 50;
                    }
                    else
                    {
                        end = x + 1;
                        break;
                    }
                }
            }
            return (start, end);
        }
    }
}
