﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SimpleDataCreator
{
    public partial class ResultForm : Form
    {
        private const int ON_PAGE = 500;

        private bool _showJumpFloor;
        private int _page;

        public ResultForm()
        {
            InitializeComponent();
        }

        private void ResultForm_Load(object sender, EventArgs e)
        {
            GenerateGrid();
        }

        private void buttonSwitch_Click(object sender, EventArgs e)
        {
            _showJumpFloor = !_showJumpFloor;
            _page = 0;
            buttonSwitch.Text = _showJumpFloor ? "🏄" : "🏃";
            dataGridView.Columns[3].Visible = dataGridView.Columns[4].Visible = !_showJumpFloor;
            dataGridView.Columns[7].Visible = _showJumpFloor;
            GenerateGrid();
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            if (_page > 0)
            {
                _page--;
                GenerateGrid();
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            _page++;
            GenerateGrid();
        }

        private void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                int id = int.Parse(dataGridView.SelectedRows[0].Cells[0].Value.ToString());
                Frame frame = _showJumpFloor ? (Frame)Form1.JumpFrames.First(f => f.Id == id) : Form1.FloorFrames.First(f => f.Id == id);
                pictureBox.Image = Image.FromFile(frame.FilePath);
            }
        }

        private void GenerateGrid()
        {
            dataGridView.Rows.Clear();
            if (_showJumpFloor)
            {
                foreach (var frame in Form1.JumpFrames.Skip(_page * ON_PAGE).Take(ON_PAGE).Take(ON_PAGE))
                    dataGridView.Rows.Add(frame.ToDataRow());
            }
            else
            {
                foreach (var frame in Form1.FloorFrames.Skip(_page * ON_PAGE).Take(ON_PAGE).Take(ON_PAGE))
                    dataGridView.Rows.Add(frame.ToDataRow());
            }
        }
    }
}
