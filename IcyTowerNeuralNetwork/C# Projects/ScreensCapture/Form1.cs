﻿using SimpleDataCreator;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebSocketSharp;

namespace Desktop
{
    public partial class Form1 : Form
    {
        private const int IT_WINDOW_WIDTH = 642;
        private const int IT_WINDOW_HEIGHT = 507;
        private const string DIRECTORY_BASE = @"E:\Simple Data";

        private Process _icyTowerProccess;
        private CancellationTokenSource _cancellationToken;
        private Stopwatch _stopwatch = new Stopwatch();
        private Task _recordingTask;

        private bool _analyze;
        private Bitmap _screen;
        private Frame _prevFrame;
        private int _index;
        private int _screenY;
        private int _screenX;
        private WebSocket _webSocket;
        private string _directory;

        #region WinForms

        public Form1(bool analyze)
        {
            InitializeComponent();
            if (!DesignMode)
            {
                _analyze = analyze;
                labelFPS.Visible = label1.Visible = false;
                labelPort.Visible = textBoxPort.Visible = _analyze;
                labelDirectory.Visible = labelS.Visible = textBoxDirectory.Visible = !_analyze;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void numericUpDownX_ValueChanged(object sender, EventArgs e)
        {
            GeneratePreview();
        }

        private void numericUpDownY_ValueChanged(object sender, EventArgs e)
        {
            GeneratePreview();
        }

        private void GeneratePreview()
        {
            Graphics graphics = panelPreview.CreateGraphics();
            graphics.CopyFromScreen((int)numericUpDownX.Value, (int)numericUpDownY.Value, 0, 0, new Size(IT_WINDOW_WIDTH, IT_WINDOW_HEIGHT));
        }

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hWnd);

        private void buttonStart_Click(object sender, EventArgs e)
        {
            _icyTowerProccess = _icyTowerProccess ?? Process.GetProcessesByName("icytower13").FirstOrDefault();
            if (_icyTowerProccess != null)
            {
                _cancellationToken = new CancellationTokenSource();

                buttonStart.Enabled = false;
                buttonStop.Enabled = true;
                labelFPS.Visible = label1.Visible = true;
                
                _screen = new Bitmap(IT_WINDOW_WIDTH, IT_WINDOW_HEIGHT);
                _index = 0;
                _screenX = (int)numericUpDownX.Value;
                _screenY = (int)numericUpDownY.Value;

                if (_analyze)
                {
                    StartAnalyzing();
                }
                else
                {
                    StartRecording();
                }
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            _cancellationToken.Cancel();
            if (_analyze)
            {
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
            }
            else
            {
                _recordingTask.Wait();
                textBoxDirectory.Text = (int.Parse(textBoxDirectory.Text) + 1).ToString("D3");
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
                //Process.Start(DIRECTORY_BASE);
                //Close();
            }
        }

        #endregion

        #region Recording

        private void StartRecording()
        {
            _index = 0;
            _directory = Path.Combine(DIRECTORY_BASE, "S" + textBoxDirectory.Text);

            Directory.CreateDirectory(_directory);
            SetForegroundWindow(_icyTowerProccess.MainWindowHandle);
            SendKeys.SendWait(" ");

            _recordingTask = Task.Factory.StartNew(Record, _cancellationToken.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        private void Record()
        {
            while (!_cancellationToken.IsCancellationRequested)
            {
                _stopwatch.Restart();
                MakeScreenShoot();
                _screen.Save($"{_directory}\\{_index}.png");
                _index++;

                long time = _stopwatch.ElapsedMilliseconds;
                //Invoke(new Action(() => labelFPS.Text = $"{1000 / time}"));
            }
        }

        #endregion
        
        #region Analyzing

        private void StartAnalyzing()
        {
            _webSocket = new WebSocket("ws://127.0.0.1:11236");
            _webSocket.Connect();

            MakeScreenShoot();
            _prevFrame = Analyzer.AnalyzeImage(_screen, null);

            Thread.Sleep(2000);
            Task.Factory.StartNew(Analyze, _cancellationToken.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        private void Analyze()
        {
            _webSocket.OnMessage += (s, e) =>
            {
                if (!_cancellationToken.IsCancellationRequested)
                {
                    _stopwatch.Restart();

                    _prevFrame.Key = (OutputKey)int.Parse(e.Data);

                    MakeScreenShoot();

                    var makeScreenShootTime = _stopwatch.ElapsedMilliseconds;

                    var frame = Analyzer.AnalyzeImage(_screen, _prevFrame, isReplay: false);
                    if (frame != null) _prevFrame = frame;

                    var makeAnalyzeTime = _stopwatch.ElapsedMilliseconds - makeScreenShootTime;

                    string frameToSend = _prevFrame.ToNeuralNetworkRow();
                    _webSocket.Send(frameToSend);

                    long totalTime = _stopwatch.ElapsedMilliseconds;
                    Invoke(new Action(() => labelFPS.Text = $"{1000 / totalTime}"));

                    Debug.WriteLine(++_index + ". " + totalTime + "ms (" + makeAnalyzeTime + " for analyze, " + (totalTime - makeAnalyzeTime - makeScreenShootTime) + " for message) | " + frameToSend);
                }
            };

            string firstFrameToSend = _prevFrame.ToNeuralNetworkRow();
            _webSocket.Send(firstFrameToSend);
        }
           
        #endregion

        #region Utils

        private void MakeScreenShoot()
        {
            using (var graphics = Graphics.FromImage(_screen))
            {
                graphics.CopyFromScreen(_screenX, _screenY, 0, 0, new Size(IT_WINDOW_WIDTH, IT_WINDOW_HEIGHT));
            }
        }

        #endregion
    }
}