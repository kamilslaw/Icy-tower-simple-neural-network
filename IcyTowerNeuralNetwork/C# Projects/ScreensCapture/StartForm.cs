﻿using Desktop;
using System;
using System.Windows.Forms;

namespace ScreensCapture
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
        }

        private void buttonCapture_Click(object sender, EventArgs e)
        {
            new Form1(false).Show();
        }

        private void buttonAnalyze_Click(object sender, EventArgs e)
        {
            new Form1(true).Show();
        }
    }
}
