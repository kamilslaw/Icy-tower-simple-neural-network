import numpy as np
import pyautogui as gui
import operator
import tensorflow as tf
import tflearn as tfl
from websocket_server import WebsocketServer
#import random

TCP_IP = '127.0.0.1'
TCP_PORT = 11236
KEYS = [['none'], ['a'], ['d'], ['space'], ['space', 'a'],
        ['space', 'd']]  # 'a' & 'd' stand for left and right
KEYS_NAMES = ['No key', 'Left', 'Right', 'Space', 'SpaceLeft', 'SpaceRight']


def get_model(classes_count, attributes_count):
    net = tfl.input_data(shape=[None, attributes_count])
    net = tfl.fully_connected(net, 40)
    net = tfl.fully_connected(net, 40)
    net = tfl.fully_connected(net, classes_count, activation='softmax')
    net = tfl.regression(net)
    return tfl.DNN(net)


# Load models
model_floor = get_model(6, 15)
model_floor.load('Models/SimpleFloorData.tfl', weights_only=True)
tf.reset_default_graph()
model_jump = get_model(3, 19)
model_jump.load('Models/SimpleJumpData.tfl', weights_only=True)

#
last_key = 0
no = 0


def new_message(client, server, data):
    global last_key
    global no
    no += 1
    # Get data
    data = np.array([int(x) for x in data.split(',')],
                    dtype=np.int16)
    is_on_floor = data[0] == 1
    network_data = data[1:]  # Remove first element (row type)
    # Find key
    model = model_floor if is_on_floor else model_jump
    [pred] = model.predict([network_data])
    key, _ = max(enumerate(pred), key=operator.itemgetter(1))
    #if key == 0 and is_on_floor and data[1] == 0:
    #    key = 3
    #    key = random.randint(1, 3)
    # Press key
    if last_key != 0 and last_key != key:
        for k in KEYS[last_key]:
            gui.keyUp(k)
    if key != 0:
        for k in KEYS[key]:
            gui.keyDown(k)
    last_key = key
    server.send_message(client, str(key))
    # Info
    print(str(no) + ".", end=' ')
    print(np.array2string(data, precision=0, separator=',', suppress_small=True))
    print(' #', end=' ')
    print(KEYS_NAMES[key])


# Start WebSocket Server
server = WebsocketServer(TCP_PORT, host=TCP_IP)
server.set_fn_message_received(new_message)
server.run_forever()
