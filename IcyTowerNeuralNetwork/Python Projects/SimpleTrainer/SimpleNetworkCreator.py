import numpy as np
import tflearn as tfl
from tflearn.data_utils import load_csv


def preprocess(data, columns_to_ignore):
    for id in sorted(columns_to_ignore, reverse=True):
        [r.pop(id) for r in data]
    return np.array(data, dtype=np.int16)


#classes_count = 3
#attributes_count = 19
#filename = 'SimpleJumpData'
#to_ignore = []
classes_count = 6
attributes_count = 15
filename = 'SimpleFloorData'
to_ignore = [4, 5, 6, 7, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24, 25, 26]

data, labels = load_csv(filename + '.csv',
                        target_column=0, categorical_labels=True, n_classes=classes_count)

data = preprocess(data, to_ignore)

net = tfl.input_data(shape=[None, attributes_count])
net = tfl.fully_connected(net, 40)
net = tfl.fully_connected(net, 40)
net = tfl.fully_connected(net, classes_count, activation='softmax')
net = tfl.regression(net)
model = tfl.DNN(net)

model.fit(data, labels, n_epoch=15, batch_size=16,
          show_metric=True, shuffle=True)

model.save('Models/' + filename + '.tfl')
print("DONE")

# testData = [[1, -10, 169, 83, 0],       # 0
#             [0, -3, 185, 194, 0],       # 1
#             [17, 39, 201, -36, -16],    # 2
#             [15, 39, 137, -47, -16]]    # 2
# testData = preprocess(testData, to_ignore)
# pred = model.predict(testData)
# print(pred)
